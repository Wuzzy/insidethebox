
`sounds/button_trigger.ogg`,
`sounds/button_untrigger.ogg`,
`sounds/piston_trigger.ogg`,
`sounds/piston_untrigger.ogg`:

 Artist: Alecia Shepherd
 See: http://www.aleciashepherd.com
 Origin: https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/1245112-snowsong-the-epic-sound-pack-sound-resource-pack
 License: CC-BY

- `nexus_place.ogg`:
 Artist: Iwan 'qubodup' Gabovitch
 Page: http://qubodup.net/
 Origin: https://opengameart.org/content/impact
 License: CC0

`models/button_down.obj`,
`models/button_up.obj`,
`models/switch.obj`,
`models/switch_on.obj`: sofar CC-BY-SA-4.0

- textures: based on Isabella-II

